(function ($) {
    /***
     * Move DOM object handler
     * @returns {Templates_L8.$.fn}
     */
    $.fn.movier = function (handlerProcess_, handlerFinish_) {
        $(this[0]).on('mousedown', function (e) {
            $(this).addClass('draggable');
            $.fn.movier.z_idx = $(this).css('z-index'),
                    $.fn.movier.drg_h = $(this).outerHeight(),
                    $.fn.movier.drg_w = $(this).outerWidth(),
                    $.fn.movier.pos_y = $(this).offset().top +
                    $.fn.movier.drg_h - e.pageY,
                    $.fn.movier.pos_x = $(this).offset().left +
                    $.fn.movier.drg_w - e.pageX;
            $(this).css({
                zIndex: 1000
            });
            return true;
        }).on('mouseup mouseout', function (e) {
            if ($(this).hasClass('draggable') && void 0 !== handlerFinish_)
                handlerFinish_(this);
            $(this).removeClass('draggable');
            $(this).css('cursor', 'default');
            $(this).css('z-index', $.fn.movier.z_idx);
            e.preventDefault();
            return true;
        }).on('mousemove mouseenter', function (e) {
            $(this).css('cursor', 'move');
            if ($(this).hasClass('draggable')) {
                $(this).offset({
                    top: e.pageY + $.fn.movier.pos_y -
                            $.fn.movier.drg_h,
                    left: e.pageX + $.fn.movier.pos_x -
                            $.fn.movier.drg_w
                })
                if (void 0 !== handlerProcess_)
                    handlerProcess_(this);
            }
            e.preventDefault();
            return true;
        });
        
        return this;
    };
    
    /****
     * Data for calculate job
     * @type type
     */

    var input = {
        "start": "A",
        "end": "B",
        "links": {
            "A:C": 7,
            "A:E": 6,
            "A:H": 5,
            "C:D": 4,
            "C:E": 5,
            "E:D": 1,
            "E:F": 4,
            "H:E": 4,
            "H:G": 2,
            "D:B": 2,
            "D:F": 4,
            "F:B": 3,
            "G:F": 4,
            "G:B": 10
        }
    };
    
    var position = {
        "A" : { x: 10, y: 300 },
        "B" : { x: 300, y: 10 },
        "C" : { x: 10, y: 190 },
        "D" : { x: 100, y: 30 },
        "E" : { x: 150, y: 150 },
        "F" : { x: 240, y: 130 },
        "G" : { x: 320, y: 250 },
        "H" : { x: 150, y: 300 }
    };
    /****
     * Calculate the main screen parameters
     * @param {type} link_ - reference to link DOM
     * @returns {direct_L1.calcScreenParameters.directAnonym$0}
     */
    function calcScreenParameters(link_) {
        var vrtxs = link_.id.split(':');
        var dx = $('#' + vrtxs[0]).offset().left -
                $('#' + vrtxs[1]).offset().left,
                dy = $('#' + vrtxs[0]).offset().top -
                $('#' + vrtxs[1]).offset().top;
        return {
            dx: dx,
            dy: dy,
            L: Math.sqrt(dy * dy + dx * dx)
        };
    }
    /***
     * Draw and position of link
     * @param {type} link_ - reference to link DOM
     * @returns {direct_L1}
     */
    function linkRender(link_) {
        var params = calcScreenParameters(link_);
        var vrtxs = link_.id.split(':');
        var minX = ((params.dx > 0) ? Math.min($('#' + vrtxs[0]).offset().left,
                $('#' + vrtxs[1]).offset().left) :
                Math.max($('#' + vrtxs[0]).offset().left,
                        $('#' + vrtxs[1]).offset().left)) +
                $('#' + vrtxs[0]).outerWidth(false) / 2,
                minY = ((params.dy > 0) ? Math.min($('#' + vrtxs[0]).offset().top,
                        $('#' + vrtxs[1]).offset().top) :
                        Math.max($('#' + vrtxs[0]).offset().top,
                                $('#' + vrtxs[1]).offset().top)) +
                $('#' + vrtxs[0]).outerHeight(false) / 2,
                width = Math.abs(params.dx),
                height = Math.abs(params.dy),
                L = Math.sqrt(height * height + width * width),
                angle = 180 * Math.acos(params.dx / params.L) / Math.PI;

        if (params.dy < 0)
            angle *= -1;

        //console.log('x = ', minX, 'y = ', minY,
        //      'width = ', width, 'height = ', height, ' L = ', L, ' dx =', dx, ' dy= ', dy, ' angle = ', angle);

        $(link_).css({
            "transform-origin": 'left 50%',
            top: minY,
            left: minX,
            width: L,
            "-webkit-transform": 'rotate(' + angle + 'deg)'
        });

        input.links[link_.id] = parseFloat(params.L/100).toFixed(1);
        $(link_).find('p').text(input.links[link_.id]);
        
        return this;
    };
    /****
     * Update date from server script
     * @returns {direct_L1}
     */
    function updateDataFromDirect() {
        //console.log('jsonData=' + JSON.stringify(input));
        $.ajax({
            type: "POST",
            url: "./direct.php",
            data: 'jsonData=' + JSON.stringify(input),
            //async: true,
            success: function (data) {
                //console.log('success...', data);
                var result = JSON.parse(data);
                //console.log('success...', result);

                $('.line').each(function () {
                    $(this).css('background-color', 'transparent');
                });
                
                $('#' + input.end).find('.result').html(result.cost);
                
                result.path.push(input.end);

                var step = 0;
                for (var p in result.path) {
                    p = parseInt(p);
                    if (step === result.path.length - 1)
                        break;
                    var link = result.path[p] + ':' + result.path[p + 1];
                    $('.line').each(function () {
                        if (this.id === link)
                            $(this).css('background-color', '#ccc');
                    });
                    //console.log('p = ', p, ' p + 1 = ', p+1, '  ', result.path[p] + ':' + result.path[p + 1]);
                    step++;
                }
            },
            failure: function (errMsg) {
                console.log('Error reiceive data from direct...');
            }
        });
        return this;
    }
    /***
     * Handler of movie point
     * @param {type} point_ - moved point
     * @returns {direct_L1}
     */
    function movieHandler(point_) {
        var links = [];
        $('.line').each( function() {
            if(~this.id.indexOf(point_.id)) 
                links[links.length] = this;
        });
        for (var l in links)
            linkRender(links[l]);
        return this;
    };
    /***
     * Handler of finish drag point
     * @param {type} point_ - moved point
     * @returns {direct_L1}
     */
    function finishHandler(point_) {
        updateDataFromDirect();
        return this;
    };
    
    $(document).ready(function() {
        var zIndex = 1;
        for(var i in input.links) {
            //console.log(input.links[i], '-->', i);
            var names = i.split(':');
            //console.log('names -->', names);
            for(var n in names) {
                if(!document.getElementById(names[n])) {
                    var point = document.createElement('div');
                    $(point).attr('id', names[n]);
                    $(point).addClass('point');
                    $(point).addClass('unselectable');
                    $(point).text(names[n]);
                    $(point).css({
                        'z-index': 1000 + zIndex,
                        left: position[names[n]].x,
                        top: position[names[n]].y
                    });
                    $('body').append(point);

                    var result = document.createElement('div');
                    $(result).addClass('result');
                    $(point).append(result);
                }
            }
            var link = document.createElement('div');
            $(link).attr('id', i);
            $(link).attr('data', input.links[i]);
            $(link).addClass('line');
            $(link).addClass('unselectable');
            $(link).css('z-index', zIndex);
            $('body').append(link);
            
            $(link).append('<p>' + input.links[i] +'</p>');
            
            linkRender(link);
            
            zIndex++;
        }
        
        updateDataFromDirect();
        
        $('.point').each( function() {
            $(this).movier(movieHandler, finishHandler);
        });
    });
    
    
    
    
})(jQuery);
