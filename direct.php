<?php

/***
 * AJAX posted json
 */
if (!isset($_POST['jsonData']))
    return (0);
else 
    $data = (array) json_decode($_POST['jsonData']);

echo direct($data);

/***
 * Arrange and calculate path and cost
 * input format:
 * Array
(   
    [start] => A  ... start pueblo
    [end] => B    ... end pueblo
    [links] => Array ... links (sample: A:C -- path between A and C pueblo), and cost it's..
        (
            [A:C] => 7
            [A:E] => 6
            [A:H] => 5
            [C:D] => 4
            [C:E] => 5
            [E:D] => 1
            [E:F] => 4
            [H:E] => 4
            [H:G] => 2
            [D:B] => 2
            [D:F] => 4
            [F:B] => 3
            [G:F] => 4
            [G:B] => 10
        )
)
 * 
 */

function direct($data) {
    $vxs = [];

    foreach ($data['links'] as $key => $value) {

        list($vertex, $direction) = explode(":", $key);

        initPoint($vertex, $vxs);
        initPoint($direction, $vxs);

        if (end($vxs[$vertex]['directions'])[1] <= $value)
            $vxs[$vertex]['directions'][] = [$direction, $value];
        else
            array_unshift($vxs[$vertex]['directions'], [$direction, $value]);
    }

    $vxs[$data['start']]['cost'] = 0;

    foreach($vxs as $key => $value)
        countCost($key, $vxs);

    $retval = ['path' => [], 'cost' => $vxs[$data['end']]['cost']];

    for ($current = $data['end']; $vxs[$current]['from'] != ''; $current = $vxs[$current]['from'])
        array_unshift($retval['path'], $vxs[$current]['from']);

    /***
     * Output format:
     * Array(
     *  ['path'] => Array(A, G, H), ['cost'] => 8
     * )
     * --- path without last pueblo -- В, i.e. [end]
     */
    return json_encode($retval);
}

/***
 * Setup init point  
 * $current_   - current point
 * &$vxs_ - reference of all arranged links
 */

function initPoint($current_, &$vxs_) {
    if(!isset($vxs_[$current_])) {
        $vxs_[$current_] = [];
        $vxs_[$current_]['visited']    = false;
        $vxs_[$current_]['cost']       = 1e12;   
        $vxs_[$current_]['from']       = '';     
        $vxs_[$current_]['directions'] = [];
    }
}

/***
 * Recursive count the summary cost 
 * $current_   - current point
 * &$vxs_ - reference of all arranged links
 */

function countCost($current_, &$vxs_) {
    $v = &$vxs_[$current_];
    
    if($v['visited'])
        return;

    foreach($v['directions'] as $k => $val) {
        if($vxs_[$val[0]]['cost'] > $val[1] + $v['cost']) {
            $vxs_[$val[0]]['cost'] = $val[1] + $v['cost'];
            $vxs_[$val[0]]['from'] = $current_;
        }
    }
    $v['visited'] = true;
}

?>

